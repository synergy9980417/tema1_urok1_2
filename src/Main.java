import java.io.*;
import java.net.URL;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws IOException {


//
//        Урок 2. Практика программирования: переменные, ветвления, циклы. Введение
//        в String API. Использование готовых методов: скачиваем веб
//        -
//                страницу,
//                сохраняем в файл
//        Цель задания:
//        Разобраться в создании решений с ветвлениями, циклами,
//        переменными.
//                Задания:
//        1. Если 5 в 15 степени больше миллиарда, вывести
//                -
//«Степень это мощь. Power
//        is a power.»

        if (Math.pow(5, 15) > 1000_000_000) System.out.println("Степень это мощь. Power is a power");

//        2. Задайте переменную. Если она больше 0, вывести «позитив», если меньше 0,
//                вывести «отрицательно»

        int x1 = 0;
        if (x1 > 0) System.out.println("позитив");
        else if (x1 < 0) System.out.println("негатив");
        else System.out.println("катаклизм");


//        3. Если квадратный корень из 15 миллионов меньше 5 тысяч, вывести
//                -
//«два
//        измерения лучше, чем одно»

        if (Math.sqrt(15_000_000) < 5000) System.out.println("два измерения лучше, чем одно");


//        4. Если 2 в 10 степени меньше 512 вывести
//                -
//«Pentium 2», иначе вывести
//        -
//«ARM»

        if (Math.pow(2, 10) < 512) System.out.println("Pentium 2");
        else System.out.println("ARM");

//        5. Задать две дробных переменных. Вывести наибольшую из них.

        double d1 = 4.5, d2 = 5.5;

        if (d1 > d2) System.out.println(d1);
        else System.out.println(d2);


//        6. Задать две переменных
//                -
//                first и second. Вывести first в степени second, second в
//        степени first.
        int first = 5, second = 7;
        System.out.println(Math.pow(first, second));
        System.out.println(Math.pow(second, first));


//        7. Задать две переменных
//                -
//                икс и игрек. Вывести, что больше
//                -
//                икс в степени
//        игрек, или наоборот.
        int x = 2, y = 3;
        if (Math.pow(x, y) > Math.pow(y, x)) System.out.println(Math.pow(x, y));
        else if (Math.pow(y, x) > Math.pow(x, y)) System.out.println(Math.pow(y, x));

        //        8. Вывести числа от 1 до 100
        for (int i = 1; i <= 100; i++) System.out.println(i);

        //        9. Вывести числа от 50 до 100
        for (int i = 50; i <= 100; i++) System.out.println(i);

//        10. Вывести числа от 100 до 1
        for (int i = 100; i >= 1; i--) System.out.println(i);
//        11. Вывести числа от 0 до
//                -
//                100
        for (int i = 0; i <= 100; i++) System.out.println(i);
//        12. Задать строковую пе
//        ременную. Заменить в ней все буквы о на «обро»
        String str = "абрикос";
        System.out.println(str.replaceAll("о", "обро"));

//        13. Задать строковую переменную. Вывести ее в верхнем регистре.
        System.out.println(str.toUpperCase());
//        14. Задать строковую переменную. Заменить в ней буквы а на @, а буквы о на 0.
        String str2 = "авто";
        str2 = str2.replaceAll("а", "@");
        str2 = str2.replaceAll("о", "0");
        System.out.println(str2);
//        15. Задать две строковых переменных. Найти, какая из них длиннее.
//        (
//                Используйте .length() )
        String str3 = "12345", str4 = "123456";
        if (str3.length() > str4.length()) System.out.println(str3);
        else if (str4.length() > str3.length()) System.out.println(str4);


//        16. Задать три переменных, найти наибольшую из них
        int x2 = 1, x3 = 3, x4 = -4;
        if (x2 > x3 && x2 > x4) System.out.println("больше всех x2");
        else if (x3 > x4 && x3 > x2) System.out.println("больше всех x3");
        else if (x4 > x3 && x4 > x2) System.out.println("больше всех x4");
//        17. Напишите программу, сохраняющую в файл статью из википедии
//«Проблема 2000 года». Прочитайте её.
//        https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B0_2000_%D0%B3%D0%BE%D0%B4%D0%B0

        writeToFile(getStr("https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B0_2000_%D0%B3%D0%BE%D0%B4%D0%B0"), "/tmp/p2000.html");

//        18. Напишите программу, сохраняющую в файл статью из википедии «Дональд
//        Кнут». Пе
//        ред сохранением в файл замените все слова Кнут на Пряник
//https://ru.wikipedia.org/wiki/%D0%9A%D0%BD%D1%83%D1%82,_%D0%94%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%B4_%D0%AD%D1%80%D0%B2%D0%B8%D0%BD


        writeToFile(getStr("https://ru.wikipedia.org/wiki/%D0%9A%D0%BD%D1%83%D1%82,_%D0%94%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%B4_%D0%AD%D1%80%D0%B2%D0%B8%D0%BD").replaceAll("Кнут", "Пряник"), "/tmp/d_k.html");
//        19. Напишите программу, которая сохраняет в файл случайную статью из
//        Википедии. (Ссылку на случайную статью можно найти на вики в меню слева:
//        https://ru.wikipedia.org/wiki/%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0 )
        writeToFile(getStr("https://ru.wikipedia.org/wiki/%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0"), "/tmp/randomwiki.html");
//        20. Сложная. Напишите программу, которая сохраняет в разные файлы 50
//        случайных статей из Википедии
        for (int i = 0; i < 20; i++)
            writeToFile(getStr("https://ru.wikipedia.org/wiki/%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0"), "/tmp/randomwiki" + i + ".html");

    }


    public static String getStr(String urlStr) {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(urlStr);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = reader.readLine()) != null) result.append(line);
            reader.close();
        } catch (Exception e) {
// ...
            System.out.println(e.getMessage());
        }

        return result.toString();
    }

    public static void writeToFile(String str, String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(str);
        writer.close();
    }
}



//        Критерии оценивания:
//        1
//        балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для вывода данных
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов




